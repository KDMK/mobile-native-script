import {Component, OnInit} from "@angular/core";
import {device} from "tns-core-modules/platform";
import * as dialogs from "tns-core-modules/ui/dialogs";
import {android, ios} from "tns-core-modules/application";
import {ad} from "tns-core-modules/utils/utils";
import getApplication = ad.getApplication;

@Component({
    selector: "Home",
    moduleId: module.id,
    templateUrl: "./home.component.html"
})
export class HomeComponent implements OnInit {
    osType: any;
    context: any;
    wifiManager: any;
    wInfo: any;
    mac: any;
    uid: any;

    constructor() {

    }

    openDialog() {
        dialogs.alert({
            title: "Example",
            message: `You can use everything you would normally use in Angular and Android application. Things start to get a little bit different when you want use some specific API. But, this is a lot easier because you don't have to write Swift code for iOs and Kotlin code for Android. You just write two methods using TypeScript and determine which to call during runtime depending on OS.`,
            okButtonText: "Close dialog"
        }).then(() => {
            console.log("Dialog closed!");
        });
    }

    ngOnInit(): void {
        this.osType = getApplication();
        console.log(typeof this.osType);
        console.log(ios);

        this.context = android.context;
        console.error("Android context:" + this.context);
        this.wifiManager = android.context.getSystemService('wifi');
        this.wInfo = this.wifiManager.getConnectionInfo();
        this.mac = this.wInfo.getMacAddress();

        this.uid = device.uuid;

        console.log(device.uuid);
    }
}
